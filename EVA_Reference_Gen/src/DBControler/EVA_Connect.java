package DBControler;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 * Unfertiger Controller ... noch versuchsweise,
 * @author anthes
 * @version 0
 */
public class EVA_Connect {

	private static String user = "eva";
	private static String password = "schalke04";
	private static String dburl = "jdbc:oracle:thin:@10.57.109.66:1526:EVA1";
	
	private static Connection connection;
	
	
	private static Connection connect()throws SQLException{
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch(Exception e){
			System.out.println("DB Fehler :" + e.getMessage());
		}
		connection = DriverManager.getConnection(dburl,user,password);
		return connection;
		} 

	public static Connection getConnection() throws Exception{
		
		if(connection !=null && !connection.isClosed())
			return connection;
		connect();
		return connection;
		
		
	}
	
}
