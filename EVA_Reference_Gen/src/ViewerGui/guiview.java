package ViewerGui;

import java.sql.Connection;
import java.sql.ResultSet;

import DBControler.EVA_Connect;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.util.Callback;

public class guiview extends Application {

	@SuppressWarnings("rawtypes")
	private ObservableList<ObservableList> data;
	@SuppressWarnings("rawtypes")
	private TableView tableview;

	public static void main(String[] args) {
		launch(args);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void buildData() {

		Connection c;
		data = FXCollections.observableArrayList();
		try {
			c = EVA_Connect.getConnection();
			//String SQL = "select * from VEHICLE_OPTION_CATEGORY";
			String SQL = "select * from VEHICLEs where order_no='16260'";
			ResultSet rs = c.createStatement().executeQuery(SQL);

			for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
				final int j = i;
				
				TableColumn col = new TableColumn(rs.getMetaData().getColumnName(i + 1));

				col.setCellValueFactory(new Callback<CellDataFeatures<ObservableList, String>, ObservableValue<String>>() {
				
					public ObservableValue<String> call(
							CellDataFeatures<ObservableList, String> param) {
								return new SimpleStringProperty(param.getValue().get(j).toString());
					}
				});

				tableview.getColumns().addAll(col);

				System.out.println("Column [" + i + "] ");
				
			}

			while (rs.next()) {
				ObservableList<String> row = FXCollections.observableArrayList();

				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
						row.add(rs.getString(i) != null ? rs.getString(i) : "");
				}
				System.out.println("Row [1] added " + row);
				data.add(row);
			}

			tableview.setItems(data);

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	@SuppressWarnings("rawtypes")
	@Override
	public void start(Stage stage) throws Exception {

		tableview = new TableView();

		buildData();

		Scene scene = new Scene(tableview);

		stage.setScene(scene);
		stage.show();
	}
}
