package view;

import java.io.IOException;
import java.io.StringReader;


import org.jdom2.input.SAXBuilder;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;

public class DataExtensionReadtest {
	

public static String xmlGetInnenFarbe(String xml, boolean n){
	String[] numbers = {"5","5","5"	,"5","5","1","5","5", "2","3","4"};
	String[] word    = {"Sonstige","Sonstige","Sonstige","Sonstige","Sonstige","Schwarz","Sonstige","Sonstige","Grau","Beige","Braun"};
	
	SAXBuilder builder = new SAXBuilder();
	Document doc;
	Element e=null;
	int index;
	
	try {
		doc = builder.build(new StringReader(xml));
		e = doc.getRootElement().getChild("Data").getChild("ColorInt");
		
		if (e!=null){
			index = e.getAttribute("ID").getIntValue();
			if (n)	return(numbers[index]);
			else
				return(word[index]);
		}
	} catch (JDOMException e1) {
	
	} catch (IOException e1) {
		
	} catch (ArrayIndexOutOfBoundsException e1){
		
	}
	
	return "ccc";
	}
	
	public static void main(String[] args) throws JDOMException, IOException {
		// TODO Auto-generated method stub
		String xml = "<AddDatas><Params><Param Name=\"StructVersion\" Value=\"1\"/></Params><Data><ColorInt ID=\"5\"/><LastCamBeltChange Value=\"20010101\"/><IsColorExtXtraRelevant Value=\"0\"/><IsColorIntXtraRelevant Value=\"0\"/></Data></AddDatas>";
		
	
		System.out.println(xmlGetInnenFarbe(xml,false));

		
	}

}
