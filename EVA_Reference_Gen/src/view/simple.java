package view;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.regex.Pattern;


import DBControler.EVA_Connect;
/**
 * Generisches Lese der EVA vehicle Tabelle zum Datenabgleich
 * @author anthes
 * @version 0.1
 * 
 * Vehicle_no muss manuell gesucht werden.
 */
public class simple {
	
	
	private static String shuffle(String s){
		String help = s;
		String[] c = s.split(Pattern.quote(" "));
		if (c.length==3){
			help = "----------   " + c[0] +" " + c[2]+ " " + c[1];
		}
		return help;
	}
	public static void main(String[] args) throws SQLException, IOException {
		Statement stmt = null;
	    ResultSet rs = null;
	    String sql = "select * from vehicles";
	    Vector<String> columnNames = new Vector<String>();
	    
	    Connection conn=null;
		try {
			conn = EVA_Connect.getConnection();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		stmt = conn.createStatement();
	     rs = stmt.executeQuery(sql);
	     if (rs != null) {
	        ResultSetMetaData columns = rs.getMetaData();
	        int i = 0;
	        while (i < columns.getColumnCount()) {
	          i++;
	          columnNames.add(columns.getColumnName(i));
	        }
	    
	        while (rs.next()) {
	         
	        	System.out.println(shuffle(rs.getString("MODEL_TEXT")) + " - " + rs.getString("DESIGN_DESC"));
	        	

	        }

	      }
	          rs.close();
	          stmt.close();
	          conn.close();
		System.out.println(System.getProperty("user.dir") );
	        }
	  
	
}

	    
