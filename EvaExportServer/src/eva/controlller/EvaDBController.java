package eva.controlller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import eva.model.Vehicle;
import eva.model.VehicleOptionCategory;

public class EvaDBController {
	/**
	 * Auslesen der Daten aus der EVA Datebank
	 * Query reengierung aus EVA
	 * cars = Fahrzeuge inkl MM und Zubeh�r
	 * evoOptions EVA Zubeh�rverwaltungs Tabellen.
	 */
	private java.util.logging.Logger jlogger  = java.util.logging.Logger.getLogger(Class.class.getName());
	private List<Vehicle> cars;
	private List<VehicleOptionCategory> evoOptions;
	private List<IWriter> writers = new ArrayList<IWriter>();
	
	public EvaDBController(){
	}
	
	public EvaDBController(IWriter... writer){
		for(int i=0;i<writer.length;i++) writers.add(writer[i]);
	}
	
	public void execute() {		
		jlogger.info("Datenverbindung zur EVA DB ge�ffnet");
		final String PU = "EVA_reader";
		EntityManagerFactory ef = Persistence.createEntityManagerFactory(PU);
	
		EntityManager em = ef.createEntityManager();

		String query = "SELECT l from Vehicle l where (((l.doPublishData)=1) AND ((l.vehSysType)=1) AND ((l.isused)=1) AND ((l.reserveState)is NULL) AND ((l.baseVehicleNo)is NULL) AND ((l.purdate)is NULL) OR ((l.doPublishData)=1) AND ((l.vehSysType)=1) AND ((l.isused)=1) AND ((l.reserveState)=0) AND ((l.baseVehicleNo)is NULL)  AND ((l.purdate)is NULL) OR ((l.doPublishData)=1) AND ((l.vehSysType)=1) AND ((l.isused)=1) AND ((l.reserveState)=1) AND ((l.baseVehicleNo)is NULL) AND ((l.purdate)is NULL)  )";

		TypedQuery<Vehicle> car = em.createQuery(query, eva.model.Vehicle.class);

		cars = car.getResultList();
		
		TypedQuery<VehicleOptionCategory> evo = em.createNamedQuery("VehicleOptionCategory.findAll", eva.model.VehicleOptionCategory.class);
		
		evoOptions =  evo.getResultList();
		
		em.close();
		ef.close();
		jlogger.info("Datenverbindung zur EVA geschlossen");
		jlogger.info("Schreiben der Ausgabedateien");
		callListener();
	}
	
	private void callListener(){
		for(IWriter l : writers) l.writer(cars, evoOptions);
	}
	
	
	public void registerWriter(IWriter writer){
		writers.add(writer);
	}

}
