package eva.controlller;

import java.util.List;

import eva.model.Vehicle;
import eva.model.VehicleOptionCategory;

/**
 * Ausgabeklassen registrieren
 * @author anthes
 *
 */
public interface IWriter {
	public void writer(List<Vehicle> cars, List<VehicleOptionCategory> evoOptions);
}
