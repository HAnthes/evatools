package modultests;

import java.io.IOException;
import eva.controlller.EvaDBController;
//import output.access.AccessWriter;
import output.csv.CSVWriter;
import output.pictures.PictureWriter;
import util.Setup;

public class ListenercsvWriter {
	
	public static void main(String[] args) {
		
		
		//Laden von Testeinstellungen
		try {
			Setup.makeSetup("D:/EvaTools/eva.properties");
		} catch (IOException e) {
			e.printStackTrace();
		}
		EvaDBController en = new EvaDBController();
		
		//CSV Erzeugen
		en.registerWriter(new CSVWriter());
		
		//Accessdatei erzeugen
		//en.registerWriter(new AccessWriter());
		
		//Bilder erzeugen
		//en.registerWriter(new PictureWriter());
		en.execute();
	
		
		
	}

}
