package output.access;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;

import util.RemoteWriter;
import util.Setup;
import eva.controlller.IWriter;
import eva.model.Vehicle;
import eva.model.VehicleOptionCategory;

/**
 * Erzeugt eine neue bef�llte Access DB aus einer vorlagen Datei.
 * @author anthes
 * 
 * @version 1 27.11.2015	
 */
public class AccessWriter implements IWriter {
	private static java.util.logging.Logger jlogger = java.util.logging.Logger.getLogger(Class.class.getName());
	private static RemoteWriter rw = RemoteWriter.getInstance();
	
	@Override
	public void writer(List<Vehicle> cars,	List<VehicleOptionCategory> evoOptions) {
		
		List<DTOCar> webCar = new ArrayList<DTOCar>();
		cars.forEach(car->webCar.add(new DTOCar(car)));
			
		try {
			Files.copy(Paths.get(Setup.getAccessTempl()), Paths.get(Setup.getAccessWork()));
			jlogger.info("Lade Ucanaccersstreiber");
			try {
				Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			} catch (ClassNotFoundException e) {
				jlogger.warning("Fehler beim laden des Treibers : " + e);
			}
			jlogger.info("DB verbinden");
			
			try (Connection conn = DriverManager.getConnection("jdbc:ucanaccess://"+Setup.getAccessWork())) {
		
				rw.print("UCanAccess : Datenbank Access Schreiben");
				DBCar.insert(conn, webCar);
				DBevo_Category.insert(conn, evoOptions);
				} catch (Exception e) {
					jlogger.warning("Fehler beim DB verbringen : " +e );
				} 
			} catch (IOException e) {
				jlogger.warning("Fehler beim erstellen des AccessTemplates" + Setup.getAccessTempl() + " " + Setup.getAccessWork());
			}
	}

}
