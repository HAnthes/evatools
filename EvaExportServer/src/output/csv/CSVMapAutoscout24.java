package output.csv;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Pattern;

import eva.model.Fextra;
import eva.model.Vehicle;
import util.Setup;

/**
 * Car -> CSV String
 * Mappt Vehicles nach CSV : Beschreibunng AS24 Schnittstelle 2015
 * @author anthes
 * @version 1, 03.01.2017
 */

public class CSVMapAutoscout24 {
	
	private static String csvLine = "";
	private static String as24TextAnfang = "";  
	private static String as24TextEnde = ";";
	private static List<String> categorie = null;
	private static GregorianCalendar now = null;
	private static String beschreibung = null;

	public static String map(Vehicle car){
		csvLine="";
		beschreibung="";
		categorie = new ArrayList<String>();
		now = new GregorianCalendar();
		beschreibung(car);
		 aa(car);
		 ba(car);
		 ca(car);
		 da(car);
		 ea(car);
		csvLine += System.getProperty("line.separator");
		return getCSV();
	}

	/*
	 * Siehe CSVMapMobile
	 * Kleine Unterschiede zwischen Mobile und AS24 
	 */
	private static void beschreibung(Vehicle car){
		int counter=1;
		int brTrigger=CSVMapUtils.gruppierung(car.getFextras().size());
		for (Fextra x : car.getFextras()) {
			beschreibung += "*" + x.getDescr() + "\\\\";
			counter++;
			if (counter > brTrigger){
				beschreibung += "\\\\";
				counter = 1;
			}
			
			if (x.getCategory() != null) {
				String[] help = x.getCategory().substring(2, x.getCategory().length()).split(Pattern.quote("|"));
				for (int a = 0; a < help.length; a++) {
					categorie.add(help[a]);
				}
			}
		}
	}

	
	/*
	 * Kraftstoffkonverter nach AS24 Beschreibung, EVA Daten per DB Auswertung
	 * @param evak
	 * @return
	 */
	private static String kraftstoff(int evak) {

		switch (evak) {
		case -1:
			return "Benzin";
		case -2:
			return "Diesel";
		case -3:
		case -9999:
		case -20:
		case -21:
			return "Sonstige";
		case -4:
			return "Elektro";
		case -5:
		case -6:
			return "Autogas (LPG)";
		case -7:
		case -9:
			return "Erdgas (CNG)";
		case -8:
			return "Elektro/ Benzin";
		case -10:
			return "Elektro/ Diesel";
		case -11:
		case -13:
		case -14:
			return "Wasserstoff";
		default:
			return "Sonstige";
		}

	}

	/*
	 * Farbnamenkonverter nach AS24
	 * @param color
	 * @return
	 */
	private static String genColor(String color) {
		if (color.equals("BEI"))
			return "Beige";
		if (color.equals("GEL"))
			return "Gelb";
		if (color.equals("BLA"))
			return "Blau";
		if (color.equals("BRA"))
			return "Braun";
		if (color.equals("BRO"))
			return "Bronze";
		if (color.equals("GOL"))
			return "Gold";
		if (color.equals("GRA"))
			return "Grau";
		if (color.equals("GRU"))
			return "Gr�n";
		if (color.equals("ORA"))
			return "Orange";
		if (color.equals("ROT"))
			return "Rot";
		if (color.equals("SCH"))
			return "Schwarz";
		if (color.equals("SIL"))
			return "Silber";
		if (color.equals("VIO"))
			return "Violett";
		if (color.equals("WEI"))
			return "Wei�";
		return "";
	}

	/*
	 * Fahrzeugkategoeriekonverter nach AS24 Beschreibung
	 * @param fzt
	 * @return
	 */
	private static String fzKat(BigDecimal fzt) {

		int type = fzt.intValue();
		if (type == 1 || type == 8 || type == 9 || type == 10)
			return "Sonstiges PKW";
		if (type == 2)
			return "Limousine";
		if (type == 3)
			return "Kombi";
		if (type == 4)
			return "Cabrio/Roadster";
		if (type == 5 || type == 26)
			return "Sportwagen/Coupe";
		if (type == 6 || type == 19)
			return "SUV/Gel�ndewagen";
		if (type == 27)
			return "Kleinwagen";
		if (type == 7)
			return "Van/Kleinbus";
		return "Sonstiges PKW";
	}
	
	/*
	 * Hilfsmethode f�r das erzeugen der CSV Zeile.
	 * @param d
	 */
	private static void addData(String d) {
		csvLine += as24TextAnfang + d + as24TextEnde;
	}

	
	/* 
	 * Modelbezeichnung in AS24 Golf IV Variant muss zu Golf Var.. IV werden auch Cabrio
	 * vorl�uft einfache umdrehen bei 3 Modellsubstrings - noch keine bessere L�sung
	 * @param s
	 * @return
	 */
	private static String shuffle(String s){
		String help = s;
		String[] c = s.split(Pattern.quote(" "));
		if (c.length==3){
			help = c[0] +" " + c[2]+ " " + c[1];
		}
		return help;
	}
	
	/*
	 * Erste Staffel EVA AS24 umsetzung.
	 * @param car
	 */
	private static void aa(Vehicle car) {

		//A AS KD NR
		addData("5785");
		
		//B Angebotsnr. 
		addData("" + car.getVehicleNo());
		
		//C BilderNamen
		addData("" + car.getVehicleNo());
	
		//D Marke
		addData(car.getText1().equals("VW NFZ") ? "VW" : car.getText1());
		
		//E Modell
		addData(shuffle(car.getModelText()));
		
		//F Version
		String modell = car.getDesignDesc();
		modell = modell.substring(0, modell.length() > 49 ? 47 : modell.length());
		addData(modell);
				
		//G Kategorie Gebrauchtwagen oder Jahreswagen bis 24 = 700 Tage monate 1 besitzer
		if (((now.getTimeInMillis() - car.getRegdate().getTime())/ 86400000) > 700 && car.getOwnerCount().equals(new BigDecimal(1))){
		addData("Jahreswagen");
		}else{
			addData("Gebrauchtwagen");
		}
	
		//H Getriebe
		if (car.getAuto().toString().equals("0")) 
			addData("Handschaltung");
		if (car.getAuto().toString().equals("1"))
			addData("Automatik");
		if (car.getAuto().toString().equals("2"))
			addData("Halbautomatik");
			
		//I Karosserieform
		addData(fzKat(car.getVehType()));
		
		//J Au�enfarbe Name
		addData(car.getColorExtText());
				
		//K Metallic Boolean
		addData(CSVMapUtils.orIn(categorie, "Ap", "AP"));
		//L W�hrung
		addData("EUR");
		
		//M Kraftstoff ..... aktuell
		addData(kraftstoff(car.getMotorId().intValue()));
		
		//N Erstzulassung
		if (car.getRegdate() != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("MM.yyyy");
			addData(formatter.format(car.getRegdate()));
		} else {
			addData("");
		}
		
		//O KM STand
		addData(car.getSpeedoKm().toString());
		
		//P Endpreis Brutto
		if (car.getBusinessType().equals(new BigDecimal(1))){
			//Diffbesteuert
			addData(car.getPriceSell().setScale(0, RoundingMode.HALF_UP).toString());
		} else{
			//Regel
			addData(car.getPriceSell().multiply(new BigDecimal(1.19)).setScale(0, RoundingMode.HALF_UP).toString());
		}
		
		//Q Anzahl der T�rne
		addData(car.getDoors().toString());
		
		//R Sitze .... hammernet 
		addData(car.getCarSeats().toString());
		
		//S Hubraum
		addData(car.getCapacity().toString());
		
		
		//T Leistung 
		addData(car.getPower().toPlainString());
		
		//U Zylinder
		addData(car.getCylinders().intValue()==0 ? "" : car.getCylinders().toString());
		
		//V G�nge
		addData("");
		
		//W Reserviert
		addData("");
		
		//X Leergewicht!
		addData("");
		
		//Y HU
		addData(car.getMainCheck() == null || car.getMainCheck().equals("NEU") ? "" : car.getMainCheck());
		
		//Z HSN
		addData(car.getHsn());
		
		//AA TSN
		addData(car.getTsn());
				
		//AB Reserviert
		addData("");
		
		//AC Garantiedauer
		addData("");
				
		//AD Mwst 1 auwseisbat 0 nicht
		addData(car.getBusinessType().toString().equals("1")?"0":"1");
		
		//AE Unfallfahrzeug
		addData("");
		
		//AF Klima
		addData(CSVMapUtils.orIn(categorie,"Cp","Ak"));
		
		//AG Klimaautomatik
		addData(CSVMapUtils.orIn(categorie,"AC"));
		
		//AH Reserviert
		addData("");
		
		//AI el. Fensterheber
		addData(CSVMapUtils.orIn(categorie,"AE"));

		//AJ Navigation
		addData(CSVMapUtils.orIn(categorie,"AB","Cd","ah"));
		
		//AK el Sitze
		addData(CSVMapUtils.orIn(categorie,"ag","A9"));
		
		//AL Schiebedach
		addData(CSVMapUtils.orIn(categorie,"Bk","AM"));
		
		//AM Sitzheizung
		addData(CSVMapUtils.orIn(categorie,"aq","ar","Au"));
		
		//AN Radio
		addData(CSVMapUtils.orIn(categorie,"aw","Ar","Cd"));		
		
		//AO ABS
		addData(CSVMapUtils.orIn(categorie,"AA"));
		
		//AP Airbag
		addData(CSVMapUtils.orIn(categorie,"af","Ad","Af"));
		
		//AQ Beifahrerairbag
		addData(CSVMapUtils.orIn(categorie,"Bv","Ad"));
		
		//AR Seitenairbags
		addData(CSVMapUtils.orIn(categorie,"Ay"));
		
		//AS Xenon
		addData(CSVMapUtils.orIn(categorie,"AO"));	
		
		//AT Zwentralverriegelung
		addData(CSVMapUtils.orIn(categorie,"AZ","AI"));
		
		//AU Alarmanlage nicht �ber EVA Auswertbar
		addData("");
		
		//AV Wegfahrsperre
		addData(CSVMapUtils.orIn(categorie,"AW"));
		
		//AW Traktionskontrolle
		addData(CSVMapUtils.orIn(categorie,"Ao"));
		
		//AX Reserviert
		addData("");
		
		//AY Reserviert
		addData("");
		
		//AZ Alufelgen
		addData(CSVMapUtils.orIn(categorie,"Al","Yl"));
	}
	
	/*
	 * Zweite Staffel EVA AS24 Umsetzung
	 * @param car
	 */
	private static void ba(Vehicle car) {	
		//BA Dachreling
		addData(CSVMapUtils.orIn(categorie,"AQ"));
		
		//BB Bordcomputer
		addData(CSVMapUtils.orIn(categorie,"Ab"));
		
		//BC Einpackhilfe
		addData(CSVMapUtils.orIn(categorie,"BF","A3","BK"));
		
		//BD Nebelscheinwerfewr
		addData(CSVMapUtils.orIn(categorie,"AN"));	
		
		//BE Servo
		addData(CSVMapUtils.orIn(categorie,"Ax"));
		
		//BF Reserviert
		addData("");
	
		//BG Beschreibung
		beschreibung += Setup.getAutoscoutCSVText();
		//04.02.2016 : Wegen dem EA189 Problem und f�r eine Erg�nzgung der Anzeige wurde "�ffentliche" Hinweise angeh�ngt
		if (car.getPublicHints()!=null) beschreibung += "\\\\" + car.getPublicHints();
		addData(beschreibung);
	
		//BH H�ndlerpreis
		addData("");
		
		//BI Reserviert
		addData("");
		
		//BJ Nur f�r H�ndler
		addData("0");
		
		//BK Artikel ID 
		addData("");
		
		//BL Anh�ngerkupplung
		addData(car.getTrailCoupl().toString());
		
		//BM ESP
		addData(CSVMapUtils.orIn(categorie,"Ao"));
		
		//BN Tempomat
		addData(CSVMapUtils.orIn(categorie,"AT"));
		
		//BO Behindertengerecht
		addData("");
		
		//BP Dekrasiegel
		addData("");
		
		//BQ Verbrauch innerorts
		addData(car.getConsumpCity().toString());
		
		//BR Verbrauch auserorts
		addData(car.getConsumpRoad().toString());
		
		//BS Verbrauch mix
		addData(car.getConsumpMix().toString());
		
		//BT Co2 mix
		addData(car.getEmissCo2Mix().toString());
		
		//BU Gebrauchtwagengarantie
		addData("");
		
		//BV Herstellergarantiesiegel
		addData("");
		
		//BW Fin Rate
		addData("");
		
		//BX Fin Dauer
		addData("");
		
		//BY Fin Anz
		addData("");
		
		//BZ Fin Zins
		addData("");		
	}
	
	/*
	 * Dritte Staffel EVA AS24 Umsetzung
	 * @param car
	 */
	private static void ca(Vehicle car){
		//CA Fin Art
		addData("");
		
		//CB Fin Schlussrate
		addData("");
		
		//CC Untertitel 
		//todo 
		addData("");
		
		//CD VIN
		addData(car.getChassisno());
		
		//CE NAP
		addData("");
		
		//CF BOVAG
		addData("");
		
		//CG Standheizung
		addData(CSVMapUtils.orIn(categorie,"AS"));
		//CH DPF
		addData(CSVMapUtils.orIn(categorie,"Bz"));
		
		//CI Schadstoffklasse
		switch (car.getEmisStandard().intValue()) {
		case 1:
			addData("EURO 1");
			break;
		case 2:
			addData("EURO 2");
			break;
		case 3:
			addData("EURO 3");
			break;
		case 4:
			addData("EURO 3");
			break;
		case 5:
			addData("EURO 4");
			break;
		case 6:
			addData("EURO 4");
			break;
		case 7:
			addData("EURO 5");
			break;
		case 8:
			addData("EURO 6");
			break;
		default:
			addData("");
			break;
		}
		
		//CJ Feinstaub
		switch (car.getEmisBadge().intValue()) {
		case 1:
			addData("");
			break;
		case 2:
			addData("Gr�n");
			break;
		case 3:
			addData("Gelb");
			break;
		case 4:
			addData("Rot");
			break;
		default:
			addData("");
			break;
		}
		
		//CK Vorbesitzer
		addData(car.getOwnerCount().toString());
		
		//CL LEistung
		addData(car.getPower().toString());
		
		//CM
		addData("");
		
		//CN Au�engrundfarbe
		addData(genColor(car.getColorExtGeneric()));
		
		//CO Bruttodarhlensbetrag
		addData("");

		//CP Bearbeitungsgeb�hren
		addData("");
		
		//CQ Ratenabsicherung
		addData("");
		
		//CR Bank
		addData("");
		
		//CS Sollzinssatz pa
		addData("");
		
		//CT Sollzinsart
		addData("");
		
		//CU Reserviert
		addData("");
		
		//CV Reserviert
		addData("");
		
		//CW Reserviert
		addData("");
		
		//CX Reserviert
		addData("");
		
		//CY Reserviert
		addData("");
		
		//CZ Reserviert
		addData("");
	}
	
	/*
	 * Vierte Staffel EVA AS24 Umseztung
	 * @param car
	 */
	private static void da(Vehicle car){
		//DA - DH nur Motorr�der
		addData("");  //DA
		addData("");  //DB
		addData("");  //DC
		addData("");  //DD
		addData("");  //DE
		addData("");  //DF
		addData("");  //DG
		addData("");  //DH
		
		//DI Scheckheftgep. ( 1 bei 1 sonst 0 ) 
		addData(car.getCareStatus().intValue()==1?"1":"0");
		
		//DJ CarPass
		addData("0");
		
		//DK Produkt 1 - Premium
		
		if(CSVMapUtils.dayDiff(car.getDateCreated().getTime())>=Setup.getAs24PremiumStart() && CSVMapUtils.dayDiff(car.getDateCreated().getTime()) <=Setup.getAs24PremiumEnde() && car.getVehiclesMms().size()>0 ){
			addData("1");
			Setup.incCountAS24Premium();
		} else {
			addData("0");
		}
	
		//DL Produkt 2 - Plus
		if((CSVMapUtils.dayDiff(car.getDateCreated().getTime())>=Setup.getAs24PlusStart()) && (CSVMapUtils.dayDiff(car.getDateCreated().getTime()) <=Setup.getAs24PlusEnde()) && (car.getVehiclesMms().size()>0) ){
			addData("1");
			Setup.incCountAS24Plus();
		} else {
			addData("0");
		}
		//DM CarGarantie
		addData("0");
		
		//DN EnVKV KRaftstoff
		addData("");
		
		//DO EnVKV Energietr�ger
		addData("");
		
		//DP Co2 Eff Klasse
		addData("");
		
		//DQ Stromverbrauch
		addData("");
		
		//DR Lieferfrist
		addData("");
		
		//DS Lieferdatum
		addData("");
		
		//DT Nichtraucher
		addData("");
		
		//DU Taxi mietmwage
		addData("");
		
		//DV Sportpaket
		addData("");
		
		//DW startstop
		addData(CSVMapUtils.orIn(categorie,"BG"));
		
		//DX MF Lk
		addData(CSVMapUtils.orIn(categorie,"A7"));
		
		//DY Tagfahrlicht
		addData(CSVMapUtils.orIn(categorie,"Bx"));
		
		//DZ Sportfahrwerk
		addData(CSVMapUtils.orIn(categorie,"At"));
	}
	
	/*
	 * F�nfte Staffel EVA AS24 Umseztung
	 * @param car
	 */
	private static void ea(Vehicle car){
		//EA Sportsizte
		addData(CSVMapUtils.orIn(categorie,"AU"));
		
		//EB Kurvenlicht
		addData(CSVMapUtils.orIn(categorie,"BE"));
		
		//EC SKisack
		addData(CSVMapUtils.orIn(categorie,"A2"));
		
		//ED HU / AU neu
		addData(car.getMainCheck()!=null && car.getMainCheck().equals("NEU") ? "1" :"0"); 
	
		//EE letzter We
		addData("");
		
		//EF letzter Zahrimen
		addData("");
		
		//EG Al Gr��e
		addData("");
		
		
		//EH Antriebsart
		switch (car.getAllwheel().intValue()) {
		case 1:
			addData("allrad");
			break;
		case 2:
			addData("front");
			break;
		case 3:
			addData("heck");
			break;
		default:
			addData("");
			break;
		}
		
		//EI Innennausstattung
		String iType = "sonsitge";
		if(CSVMapUtils.isin(categorie,"BV")) iType="alcantara";
		if(CSVMapUtils.isin(categorie,"BT")) iType="velours";
		if(CSVMapUtils.isin(categorie,"BS")) iType="stoff";
		if(CSVMapUtils.isin(categorie,"BU")) iType="teilleder";
		if(CSVMapUtils.isin(categorie,"AL")) iType="volleder";
		addData(iType);
	
		//EJ Farbe Innenausstattung
		addData(CSVMapUtils.xmlGetInnenFarbe(car.getDataExtension(), false));
		
		//EK Verahdnlungsbasis
		addData("0");
		
		//EL Video
		addData("");
		
		//EM Panoramadach
		addData(CSVMapUtils.orIn(categorie,"C6"));
		
		//EN LAndesversion
		addData("");
		
		//EO Exportpreis
		addData("");
		
		//EP Elektrischerspiegel
		addData(CSVMapUtils.orIn(categorie,"AR","ab"));
		
		//EQ Bluethooth
		addData(CSVMapUtils.orIn(categorie,"bf"));
		
		//ER Head Up
		addData(CSVMapUtils.orIn(categorie,"BQ"));
		
		// ES Freisprech
		addData("");
		
		//ET MP3
		addData("");
		
		//EU ISOFix
		addData(CSVMapUtils.orIn(categorie,"BR"));
		
		//EV Lichtsensor
		addData(CSVMapUtils.orIn(categorie,"Bk"));
		
		//EW Regensonsor
		addData(CSVMapUtils.orIn(categorie,"Bk","Bb"));
		
		//EX EPH Vonr
		addData("");
		
		//EY EPH hiunten
		addData("");
		
		//EZ Kamera
		addData(CSVMapUtils.orIn(categorie,"BK"));
		
		//Fa selbslenk
		addData(CSVMapUtils.orIn(categorie,"BF"));
		
		//FB CD
		addData(CSVMapUtils.orIn(categorie,"BM","aw","Cd","Aq"));
		
		//FC  Nettodahrlehnsbetrag
		addData("");
	}
	
	private static String getCSV() {
		return csvLine;
	}

}
