package output.csv;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Pattern;

import eva.model.Fextra;
import eva.model.Vehicle;
import util.Setup;

/**
 * Car -> CSV String Mappt Vehicles nach CSV : Beschreibunng
 * http://services.mobile.de/manual/upload-interface-csv_de.html
 * 
 * @author anthes
 * @version 1, 03.01.2017
 * 
 * @since : Mobile Beschreibung mit EVA Update abgelichen - 16.01.2017
 * 
 */

public class CSVMapMobile {

	private static String csvLine = null;
	private static String mobileTextAnfang = "\"";
	private static String mobileTextEnde = "\";";
	private static List<String> categorie = null;
	private static GregorianCalendar now = null;
	private static String co2eff = "";
	private static String landeskennung = "";
	private static String neuwagen = "0";
	private static String beschreibung = null;
	private static boolean renewboolean = false;
	private static long renewdate=0;
	private static long bookingDate=0;
	
	
	public static String map(Vehicle car, List<CarDate> c) {
		
		for(CarDate x : c){
			if(x.getFzno().equals(car.getVehicleNo().toString())){
				renewboolean = x.isRenew();
				renewdate=x.getDateinmilis();
				c.remove(x);
				break;
			}			
		}
		
		if(Setup.isMobilerepeat() && renewdate !=0){
			bookingDate = renewdate;
		} else {
			bookingDate = car.getDateCreated().getTime();
		}
			
		csvLine = "";
		beschreibung = "";
		categorie = new ArrayList<String>();
		now = new GregorianCalendar();
		beschreibung(car);
		a_z(car);
		aa_az(car);
		ba_bz(car);
		ca_cz(car);
		da_ez(car);
		fa_fz(car);
		csvLine += System.getProperty("line.separator");
		return getCSV();
	}

	/*
	 * Erzeugt den Beschreibungstring. brTrigger dient zur erzeugungg von
	 * Abs�tzen die durch die Mobile.de Parameter erzeugt werden Die Liste
	 * Categorie wird bef�llt. Eva liefert die Werte als CSV �hnlichen String in
	 * der Relation zur der Ausstattung Fextras. Anhand dieser Liste werden die
	 * Mobile Felder ermittelt. Die K�rzel der Liste lassen sich durch Eva
	 * ermitteln. http://services.mobile.de/manual/formatting-syntax.html
	 */
	private static void beschreibung(Vehicle car) {
		int counter = 1;
		final int brTrigger = CSVMapUtils.gruppierung(car.getFextras().size());

		for (Fextra x : car.getFextras()) {
			beschreibung += "* " + x.getDescr() + "\\\\";
			counter++;
			if (counter > brTrigger) {
				beschreibung += "\\\\";
				counter = 1;
			}

			if (x.getCategory() != null) {
				String[] help = x.getCategory().substring(2, x.getCategory().length()).split(Pattern.quote("|"));
				for (int a = 0; a < help.length; a++) {
					categorie.add(help[a]);
				}
			}
		}
	}

	/*
	 * Krafstoffkonverter nach Mobilevorgabene
	 * 
	 * @param evak
	 * 
	 * @return
	 */
	private static int kraftstoff(int evak) {

		switch (evak) {
		case -1:
			return 1;
		case -2:
			return 2;
		case -3:
		case -9999:
		case -20:
		case -21:
			return 0;
		case -4:
			return 6;
		case -5:
		case -6:
			return 3;
		case -7:
		case -9:
			return 4;
		case -8:
		case -12:
		case -22:
		case -19:
		case -16:
		case -15:
			return 7;
		case -11:
		case -13:
		case -14:
			return 8;
		case -18:
		case -17:
			return 9;
		case -10:
			return 10;
		default:
			return 0;
		}
	}

	/*
	 * Generic Colorkonverter nach Mobilevorgaben
	 * 
	 * @param color
	 * 
	 * @return
	 */
	private static String genColor(String color) {
		if (color.equals("BEI"))
			return "Beige";
		if (color.equals("GEL"))
			return "Gelb";
		if (color.equals("BLA"))
			return "Blau";
		if (color.equals("BRA"))
			return "Braun";
		if (color.equals("BRO"))
			return "Bronze";
		if (color.equals("GOL"))
			return "Gold";
		if (color.equals("GRA"))
			return "Grau";
		if (color.equals("GRU"))
			return "Gr�n";
		if (color.equals("ORA"))
			return "Orange";
		if (color.equals("ROT"))
			return "Rot";
		if (color.equals("SCH"))
			return "Schwarz";
		if (color.equals("SIL"))
			return "Silber";
		if (color.equals("VIO"))
			return "Violett";
		if (color.equals("WEI"))
			return "Wei�";
		return "";
	}

	/*
	 * Fahrzeugkategoeireneinteilung nach Mobilevorgaben
	 * 
	 * @param fzt
	 * 
	 * @return
	 */
	private static String fzKat(BigDecimal fzt) {

		int type = fzt.intValue();
		if (type == 1 || type == 8 || type == 9 || type == 10)
			return "Car.OtherCar";
		if (type == 2)
			return "Car.Limousine";
		if (type == 3)
			return "Car.EstateCar";
		if (type == 4)
			return "Car.Cabrio";
		if (type == 5 || type == 26)
			return "Car.SportsCar";
		if (type == 6 || type == 19)
			return "Car.OffRoad";
		if (type == 27)
			return "Car.SmallCar";
		if (type == 7)
			return "Car.Van";
		return "Car.OtherCar";
	}

	/*
	 * Hilfmethode zur Erstellung des CSV Strings
	 * 
	 * @param d
	 */
	private static void addData(String d) {
		csvLine += mobileTextAnfang + d + mobileTextEnde;
	}

	/*
	 * Mobile CSV Umseztung Staffel 1 A-Z
	 * 
	 * @param car
	 */
	private static void a_z(Vehicle car) {

		// 0 A Kundennummer
		addData("" + car.getVehicleNo());

		// 1 B interne Nummer
		addData("" + car.getVehicleNo());

		// 2 C Kategorie
		addData(fzKat(car.getVehType()));

		// 3 D Marke
		addData(car.getText1().equals("VW NFZ") ? "VW" : car.getText1());

		// 4 E Modell 48 zeichen
		String modell = car.getText2() + " " + car.getDesignDesc();
		modell = modell.substring(0, modell.length() > 47 ? 47 : modell.length());
		addData(modell);

		// 5 F Leistung
		addData(car.getPower().toPlainString());

		// 6 G HU
		addData(car.getMainCheck() == null || car.getMainCheck().equals("NEU") ? "" : car.getMainCheck());

		// 7 H AU - �nderung mobile.de 01.09.2016
		addData("");

		// 8 I EZ
		if (car.getRegdate() != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("MM.yyyy");
			addData(formatter.format(car.getRegdate()));
		} else {
			addData("");
		}

		// 9 J Kilometer
		addData(car.getSpeedoKm().toString());

		// 10 K Preis
		addData(car.getPriceSell().setScale(2, RoundingMode.HALF_UP).toString());

		// 11 L Mwst
		addData(car.getBusinessType().toString());

		// 12 M Leer
		addData("");

		// 13 N Oldtimer
		addData("0");

		// 14 O VIN
		addData(car.getChassisno());

		// 15 P besch�digtes Fahrzeug
		// Unfallfrei ist nicht gesetzt und keine Reparatur
		// AccidentFree 0, ACC_REPAIRED_BY = null oder ""

		if (car.getAccidentFree().compareTo(new BigDecimal(0)) == 0
				&& (car.getAccRepairedBy() == null || car.getAccRepairedBy().equals(""))) {
			addData("1");
		} else {
			addData("0");
		}

		// 16 Q Farbe
		String farbe = car.getColorExtText() + "/" + genColor(car.getColorExtGeneric());
		addData(farbe.length() > 32 ? car.getColorExtText() : farbe);

		// 17 R Klimaanlage AC, Cp, Ak, Klimatomatik 2 sonst 1
		int help;
		if (CSVMapUtils.isin(categorie, "AC"))
			help = 2;
		else if (CSVMapUtils.isin(categorie, "Cp") || CSVMapUtils.isin(categorie, "Ak"))
			help = 1;
		else
			help = 0;
		addData("" + help);

		// 18 S Taxi
		addData("0");

		// 19 T Behindertengerecht
		addData(CSVMapUtils.orIn(categorie, "BH"));

		// 20 U Jahreswagen unter 450 Tage ab EZ!
		addData(((now.getTimeInMillis() - car.getRegdate().getTime()) / 86400000) > 450 ? "0" : "1");

		// 21 V Neuwagen aus Hints im Konstruktor
		addData(neuwagen);

		// 22 W Unsere Empfehlung
		addData("0");

		// 23 H�ndlerpreis
		if (car.getResellerPrice().compareTo(car.getPriceSell()) == -1
				&& car.getResellerPrice().compareTo(new BigDecimal(0)) == 1) {
			addData(car.getResellerPrice().multiply(new BigDecimal(1.19)).setScale(2, RoundingMode.HALF_UP).toString());
		} else {
			addData("");
		}

		// 24 Y reserviert
		addData("");

		// 25 Z Bemerkung L�ngenbeschr�nkung beachten 12.11.15 3000 Zeichen
		if (beschreibung.length() + Setup.getMobileCSVText().length() <= 3000)
			beschreibung += Setup.getMobileCSVText();
		// 04.02.2016 : Wegen dem EA189 Problem und f�r eine Erg�nzgung der
		// Anzeige wurde "�ffentliche" Hinweise angeh�ngt
		if (car.getPublicHints() != null)
			beschreibung += "\\\\" + car.getPublicHints();
		addData(beschreibung);

	}

	/*
	 * Mobile CSV Umseztung Staffel 2 AA - AZ
	 * 
	 * @param car
	 */
	private static void aa_az(Vehicle car) {

		// 26 AA Bild ID
		addData("" + car.getVehicleNo());

		// 27 AB Metallic EVA AP, Ap
		addData(CSVMapUtils.orIn(categorie, "AP", "Ap"));

		// 28 AC W�hrung nur EUR
		addData("EUR");

		// 29 MWSTSatz
		addData(car.getBusinessType().equals(new BigDecimal(0)) ? "19" : "0");

		// 30 AE Garantie
		addData("0");

		// 31 AF Leichtmetallfelgen Al, Y1
		addData(CSVMapUtils.orIn(categorie, "Al", "Yl"));

		// 32 AG ESP Ao
		addData(CSVMapUtils.orIn(categorie, "Ao"));

		// 33 AH ABS AA
		addData(CSVMapUtils.orIn(categorie, "AA"));

		// 34 AI Anh�ngerkupplung
		addData(car.getTrailCoupl().toString());

		// 35 AJ reserviert
		addData("");

		// 36 AK Wegfahersperre AW
		addData(CSVMapUtils.orIn(categorie, "AW"));

		// 37 AL Navigationssystem ah, AB, Cd
		addData(CSVMapUtils.orIn(categorie, "AB", "Cd", "ah"));

		// 38 AM Schiebedach Am,AM,Af,C6
		addData(CSVMapUtils.orIn(categorie, "Am", "AM", "Af", "C6"));

		// 39 AN Znetralveriegelung AZ,AI
		addData(CSVMapUtils.orIn(categorie, "AZ", "AI"));

		// 40 AO Fesnterheber AE
		addData(CSVMapUtils.orIn(categorie, "AE"));

		// 41 AP Allrad
		addData(car.getAllwheel().equals(new BigDecimal(0)) ? "1" : "0");

		// 42 AQ Tueren
		addData(car.getDoors().toString());

		// 43 Umweltplakette
		switch (car.getEmisBadge().intValue()) {
		case 1:
			addData("1");
			break;
		case 2:
			addData("4");
			break;
		case 3:
			addData("3");
			break;
		case 4:
			addData("2");
			break;
		default:
			addData("1");
			break;
		}

		// 44 Servolenkung Ax
		addData(CSVMapUtils.orIn(categorie, "Ax"));

		// 45 Biodiesel
		addData("0");

		// 46 AU Scheckheftgepflegt
		addData(car.getCareStatus().toString());

		// 47 - 49 nur Motorr�der
		addData("");
		addData("");
		addData("");

		// 50 AY Vorf�hrwagen
		addData("0");

		// 51 AZ nur Motorr�der
		addData("");
	}

	/*
	 * Mobile CSV Umseztung Staffel 3 BA - BZ
	 * 
	 * @param car
	 */
	private static void ba_bz(Vehicle car) {
		// 52 BA ccm
		addData(car.getCapacity().toString());
		// 53 BB - 60 BI Nutzfahrzeuge
		addData(""); // 53
		addData(""); // 54
		addData(""); // 55
		addData(""); // 56
		addData(""); // 57
		addData(""); // 58
		addData(""); // 59
		addData(car.getCarSeats().toString()); // 60 Sitze!
		// BJ Schadstoffklasse
		switch (car.getEmisStandard().intValue()) {
		case 1:
			addData("1");
			break;
		case 2:
			addData("2");
			break;
		case 3:
			addData("3");
			break;
		case 4:
			addData("3");
			break;
		case 5:
			addData("4");
			break;
		case 6:
			addData("4");
			break;
		case 7:
			addData("5");
			break;
		case 8:
			addData("6");
			break;
		default:
			addData("");
			break;
		}
		// 62 BK Kabineneart Nutz
		addData("");

		// 63 BL Achsen Nutz
		addData("");

		// 64 BM Tempomat AT
		addData(CSVMapUtils.orIn(categorie, "AT"));

		// 65 BN Standheizung AS
		addData(CSVMapUtils.orIn(categorie, "AS"));

		// 66 BO - 72 BU Nutz
		addData(""); // 66
		addData(""); // 67
		addData(""); // 68
		addData(""); // 69
		addData(""); // 70
		addData(""); // 71
		addData(""); // 72

		// 73 BV Tv AK
		addData(CSVMapUtils.orIn(categorie, "ak", "Cc"));

		// 74 BW - 77 BZ Nutz
		addData(""); // 74
		addData(""); // 75
		addData(""); // 76
		addData(""); // 77
	}

	/*
	 * Mobiel CSV Umsetzung Staffel 4 CA-CZ
	 * 
	 * @param car
	 */
	private static void ca_cz(Vehicle car) {
		// 78 CA - 92 CO Nutz.
		addData(""); // 78
		addData(""); // 79
		addData(""); // 80
		addData(""); // 81
		addData(""); // 82

		// 83 Luftfederung Bw
		addData(CSVMapUtils.orIn(categorie, "Bw"));

		addData(""); // 84
		addData(""); // 85
		addData(""); // 86
		addData(""); // 87
		addData(""); // 88
		addData(""); // 89

		// 90 K�hlung Frische Nq
		addData(CSVMapUtils.orIn(categorie, "Nq"));

		addData(""); // 91
		addData(""); // 92

		// 93 H�ndlerpreis
		if (car.getResellerPrice().compareTo(car.getPriceSell()) == -1
				&& car.getResellerPrice().compareTo(new BigDecimal(0)) == 1) {
			addData("1");
		} else {
			addData("0");
		}

		// 94 CQ reservirt
		addData("");

		// 95 envkv
		addData("0");

		// 96 CS Verbrauch innerorts
		addData(car.getConsumpCity().toString());

		// 97 CT Verbrauch auserorts
		addData(car.getConsumpRoad().toString());

		// 98 CU Verbrauch mix
		addData(car.getConsumpMix().toString());

		// 99 CV Verbrauch innerots
		addData(car.getEmissCo2Mix().toString());

		// 100 CW Xenon AO
		addData(CSVMapUtils.orIn(categorie, "AO"));

		// 101 CX Sitzheizung Au, ar, aq
		addData(CSVMapUtils.orIn(categorie, "Au", "ar", "aq"));

		// 102 CY Partikelfilter Bz
		addData(CSVMapUtils.orIn(categorie, "Bz"));

		// 103 CZ Einparkhilfe ist erseztzt
		addData("");
	}

	private static void da_ez(Vehicle car) {
		// 104 DA Schwacke Code
		addData("");

		// 105 DB Lieferdatum
		if (neuwagen.equals("1")) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
			addData(formatter.format(now.getTime()));
		} else {
			addData("");
		}

		// 106 DC Lieferfreist
		addData("");

		// 107 DD �berf�hrungskosten
		addData("");

		// 108 DE HU / AU Neumachen
		if (car.getMainCheck() != null)
			addData(car.getMainCheck().equals("NEU") ? "1" : "0");
		else
			addData("0");

		// 109 DF Kraftstoff
		addData("" + kraftstoff(car.getMotorId().intValue()));

		// 110 DG GetriebeArt
		if (car.getAuto().toString().equals("0"))
			addData("1");
		if (car.getAuto().toString().equals("1"))
			addData("3");
		if (car.getAuto().toString().equals("2"))
			addData("2");

		// 111 DH Export
		addData("");

		// 112 DI Tagrszulassung
		addData("");

		// 113 DJ Blickf�nger - Brechnung von automatischen Schaltzyklen
		// Zwischen Start und Ende und mit Bild!

		if ((CSVMapUtils.dayDiff(bookingDate) >= Setup.getMobileBlickfStart())
				&& (CSVMapUtils.dayDiff(bookingDate) <= Setup.getMobileBlickfEnde())
				&& (car.getVehiclesMms().size() > 0)) {
			addData("1");
			Setup.incCountMobileBlickf();
		} else {
			addData("0");
		}

		// 114 DK HSN
		addData(car.getHsn());

		// 115 DL TSN
		addData(car.getTsn());

		// 116 DM Seite 1 - Brechnung von automatischen Schaltzyklen
		if (CSVMapUtils.dayDiff(bookingDate) >= Setup.getMobileErsteSeiteStart()
				&& CSVMapUtils.dayDiff(bookingDate) <= Setup.getMobileErsteSeiteEnde()
				&& car.getVehiclesMms().size() > 0) {
			addData("1");
			Setup.incCountMobileErsteSeite();
		} else {
			addData("0");
		}

		// 117 DN reserviert
		addData("");

		// 118 DO reserviert
		addData("");

		// 119 DP E10
		addData("");

		// 120 DQ Qualit�tssiegel
		addData("");

		// 121 DR Pflanzen�l
		addData("");

		// 122 SCR - 128 Stra�enzulassung
		addData(""); // 122
		addData(""); // 123
		addData(""); // 124
		addData(""); // 125
		addData(""); // 126
		addData(""); // 127
		addData(""); // 128

		// 129 Etagenbett Cg
		addData(CSVMapUtils.orIn(categorie, "Cg"));

		// 130 Festbett Ch
		addData(CSVMapUtils.orIn(categorie, "Ch"));

		// 131 Heckgarage Cl
		addData(CSVMapUtils.orIn(categorie, "Cl"));

		// 132 Markisse Cm
		addData(CSVMapUtils.orIn(categorie, "Cm"));

		// 133 Dusche Cq
		addData(CSVMapUtils.orIn(categorie, "Cq"));

		// 134 Solaranalage Cn
		addData(CSVMapUtils.orIn(categorie, "Cn"));

		// 135 Mittelsitzgruppe Cj
		addData(CSVMapUtils.orIn(categorie, "Cj"));

		// 136 Rundsitzgruppe Ci
		addData(CSVMapUtils.orIn(categorie, "Ci"));

		// 137 Seitensitzgruppe
		addData(CSVMapUtils.orIn(categorie, "Ck"));

		addData("0"); // 138 Hagelschaden
		addData(""); // 139 Schlafpl�tze
		addData(""); // 140 Fahrzeugk��nge
		addData(""); // 141 Fahrzeugbreite
		addData(""); // 142 H�he
		addData(""); // 143 Laderaum Palette
		addData(""); // 144 Laderaum Volumen
		addData(""); // 145 Ladraum l�nge
		addData(""); // 146 Ladraum breite
		addData(""); // 147 Ladraum H�he

		// 148 Anzeige erneuern -Keine Funktion vorgesehen
		
		if (renewboolean) {
			addData("1");
			Setup.incCountMobileReNew();
		} else {
			addData("0");
		}

		addData(""); // 149 eff Jahreszins
		addData(""); // 150 moantliche Rate
		addData(""); // 151 laufzeit
		addData(""); // 152 Anzahlung
		addData(""); // 153 Schlussrate
		addData("0"); // 154 Fin Features

		// 155 EZ Interieurfarbe
		addData(CSVMapUtils.xmlGetInnenFarbe(car.getDataExtension(), true));
	}

	private static void fa_fz(Vehicle car) {

		// 156 FA Interuiertype
		String iType = "6";
		if (CSVMapUtils.isin(categorie, "BV"))
			iType = "5"; // Alcantara
		if (CSVMapUtils.isin(categorie, "YL") || CSVMapUtils.isin(categorie, "AL"))
			iType = "1"; // Volleder
		if (CSVMapUtils.isin(categorie, "BU"))
			iType = "2"; // Teilleder
		if (CSVMapUtils.isin(categorie, "BS"))
			iType = "3"; // Stoff
		if (CSVMapUtils.isin(categorie, "BT"))
			iType = "4"; // Velour
		addData(iType);

		// 157 FB Airbag
		iType = "";
		if (CSVMapUtils.isin(categorie, "af"))
			iType = "5";
		if (CSVMapUtils.isin(categorie, "AY"))
			iType = "4";
		if (CSVMapUtils.isin(categorie, "Ad"))
			iType = "3";
		if (CSVMapUtils.isin(categorie, "Af"))
			iType = "2";
		if (CSVMapUtils.isin(categorie, "Bv"))
			iType = "3";
		addData(iType);

		// 158 FC Vorbesitzer
		addData(car.getOwnerCount().toString());

		// 159 FFD Topinserrat
		addData("0");

		// 160 Bruttokreditbetrag
		addData("");

		// 161 Abschlussgeb�hren
		addData("");

		// 162 Ratenabsicherung
		addData("");

		// 163 Nettokreditbetrag
		addData("");

		// 164 Anbieterbank
		addData("");

		// 165 Soll-Zinssatz
		addData("");

		// 166 Art des SollZinssatzes
		addData("");

		// 167 Landesversion
		addData(landeskennung);

		// 168 Videour
		addData("");

		// 169 Energiereffz
		addData(co2eff);

		// 170 envkv_benzin
		addData(kraftstoff(car.getMotorId().intValue()) == 1 ? "SUPER" : "");

		// 171 Seitenspiegel AR ab
		addData(CSVMapUtils.orIn(categorie, "AR", "ab"));

		// 172 Sporfahrwerk
		addData(CSVMapUtils.orIn(categorie, "At"));

		// 173 Sporpaket
		addData("0");

		// 174 Bluetooth bf
		addData(CSVMapUtils.orIn(categorie, "bf"));

		// 175 Bordcomputer Ab
		addData(CSVMapUtils.orIn(categorie, "Ab"));

		// 176 CD Spieler aw, BM ,Aq
		addData(CSVMapUtils.orIn(categorie, "aw", "BM", "Aq"));

		// 177 el Sitzeinstellung A9
		addData(CSVMapUtils.orIn(categorie, "A9"));

		// 178 Head Up BQ
		addData(CSVMapUtils.orIn(categorie, "BQ"));

		// 179 Freisprech
		addData("0");

		// 180 MP3
		addData("0");

		// 181 Multi Lenkraf A7
		addData(CSVMapUtils.orIn(categorie, "A7"));

		// 182 Skisack A2
		addData(CSVMapUtils.orIn(categorie, "A2"));

		// 183 Tuner oder Radio Ar, aw, Cd
		addData(CSVMapUtils.orIn(categorie, "Cd", "Ar", "aw"));

		// 184 Sportsitze
		addData(CSVMapUtils.orIn(categorie, "AU"));

		// 185 Panorama Dach
		addData(CSVMapUtils.orIn(categorie, "C6"));

		// 186 Kindersizt
		addData(CSVMapUtils.orIn(categorie, "BR"));

		// 187 Kurvenlicht
		addData(CSVMapUtils.orIn(categorie, "BE"));

		// 188 Lichtsensor
		addData(CSVMapUtils.orIn(categorie, "Bk"));

		// 189 Nebelscheinwewefer
		addData(CSVMapUtils.orIn(categorie, "An"));

		// 190 Tagfahrlicht
		addData(CSVMapUtils.orIn(categorie, "Bx"));

		// 191 Tranktionskontrolle
		addData(CSVMapUtils.orIn(categorie, "Ae", "Ao"));

		// 192 Start Stop
		addData(CSVMapUtils.orIn(categorie, "BG"));

		// 193 Regensensor
		addData(CSVMapUtils.orIn(categorie, "Bb", "Bk"));

		// 194 nichtraucher
		addData("0");

		// 195 Dachreling
		addData(CSVMapUtils.orIn(categorie, "AQ"));

		// Unfall - Entscheidungmartix
		// http://services.mobile.de/manual/damage.html
		// 15/ Besch�digtes FZ EVA.Unfallfrei=nein und kein Text in
		// "unfallsch�den repariert durch"
		// 196 Unfallfz EVA.Unfallfrei=nein
		// 197 Fahrbereite EVA.fahrbereit
		// Unfall nicht repariert fahrbereite : Unfallfrei nein, Fahrbereit ja
		// repariert und fahrbereit : Unfallfrei nein, Text in repariert durch,
		// Fahrbereit ja
		if (car.getAccidentFree().compareTo(new BigDecimal(0)) == 0) {
			addData("1");
		} else {
			addData("0");
		}

		// 197 Fahrtauglich
		addData("" + car.getMobil());

		// 198 Produktionsdatum
		addData("");

		// 199 Einparkhilfe vorne
		addData(CSVMapUtils.orIn(categorie, "A3"));

		// 200 Einparkhilfe hinten
		addData(CSVMapUtils.orIn(categorie, "A3"));

		// 201 Einparkhilfe Kamera, Heck BK, Front BI, Seite BJ
		addData(CSVMapUtils.orIn(categorie, "BK", "BI", "BJ"));

		// 202 Einparkhilfe selbslenk
		addData(CSVMapUtils.orIn(categorie, "BF"));

		// 203 Topinserat - Nicht mehr genutzt Schnittstelle 09.11.2016
		addData("");

		// 204 Rotstifft
		addData("0");

		// 205 ebay - Brechnung von automatischen Schaltzyklen Bruttopreis und
		// KM - Setup
		// Bruttopreisberechnung!
		double bpreis = car.getPriceSell().doubleValue();
		if (car.getBusinessType().equals(new BigDecimal(0))) {
			bpreis *= 1.19;
		}

		if (CSVMapUtils.dayDiff(bookingDate) >= Setup.getMobileEbayStart()
				&& CSVMapUtils.dayDiff(bookingDate) <= Setup.getMobileEbayEnde()
				&& car.getVehiclesMms().size() > 0 && bpreis < Setup.getMobileEbayPreis()
				&& car.getSpeedoKm().compareTo(new BigDecimal(Setup.getMobileEbayKM())) > 0) {
			addData("1");
			Setup.incCountMobileEBay();
		} else {
			addData("0");
		}

		// 206 Pluginhybrid
		addData("");

		// 207 Kombinierterstromverbrauch
		addData("");

		// 208 HA Higlight 1 - 02.02.2017 - vorl�ufig fix eingestellt
		addData("Volkswagen Service");

		// 209 HB Highlight 2
		addData("");

		// 210 HC Highlight 3
		addData("");
	}

	private static String getCSV() {
		return csvLine;
	}

}
