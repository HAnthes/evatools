package output.csv;

import java.io.IOException;
import java.io.StringReader;
import java.util.GregorianCalendar;
import java.util.List;

import org.jdom2.input.SAXBuilder;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;


/**
 * Hilfmethoden f�r die CSV Umsetzung
 * @author anthes
 * @version 2 - 04.01.2017 
 */
public class CSVMapUtils {
	
	private static java.util.logging.Logger jlogger = java.util.logging.Logger.getLogger(Class.class.getName());
	
	/**
	 * Pr�ft das vorhanden sein eines Ausstattungsk�rzel in der  Ausstattungsliste
	 * zb. Innenausstattung oder Airbags
	 * @param categorie
	 * @param s
	 * @return
	 */
	public static boolean isin(List<String> categorie, String s) {
		if(categorie.contains(s))return true;
		return false;
	}
	
	/**
	 * Pr�ft das Vorhandensein bestimmter Ausstattungsmerkmal in der Ausstattungsliste
	 * F�r Boolean  CSV Felder
	 * @param categorie
	 * @param s
	 * @return
	 */
	public static String orIn(List<String> categorie, String... s ){
		for(int i=0;i<s.length;i++){
			if(categorie.contains(s[i])) return "1";
		}
		return "0";
		}

	/**
	 * Legt die Gruppierung der Beschreibungstexte fest - normal in dreier Gruppen.
	 * @param z
	 * @return
	 */
	public static int gruppierung(int z){
		int rest=0;
		rest = z % 3;
		if (rest != 0 ){ //Es geht nicht mit dreier Gruppen auf.
			if (z % 5 == 0) return 5;  //F�nfergruppen passen
			if (rest < z % 5) return 5;  // Was ist besser dreier oder f�nfer gruppen
		}
	return 3; //Dreie Gruppen...
	}
	
	/**
	 * Wieviel Tag von milis bis jetzt! F�r die Sonderfunktionen erste Seite inserat Blickf�nger
	 * @param milis
	 * @return
	 */
	public static int dayDiff(long milis) {
	 	
		GregorianCalendar today = new GregorianCalendar();
		GregorianCalendar past = new GregorianCalendar();
		
		past.setTimeInMillis(milis);

	    long difference = today.getTimeInMillis() - past.getTimeInMillis();
	    return (int)(difference / (1000 * 60 * 60 * 24));
	 }


	/**
	 * Ermittelt die Innenfarbe in aus dem ExtionsDatenfeld der EVA
	 * @param xml
	 * @param n : true mobile nummer, false as24 Text
	 * @return
	 */
	public static String xmlGetInnenFarbe(String xml, boolean n){
		String[] numbers = {"5","5","5"	,"5","5","1","5","5", "2","3","4"};
		String[] word    = {"Sonstige","Sonstige","Sonstige","Sonstige","Sonstige","Schwarz","Sonstige","Sonstige","Grau","Beige","Braun"};
		
		SAXBuilder builder = new SAXBuilder();
		Document doc;
		Element e=null;
		int index;
		
		try {
			doc = builder.build(new StringReader(xml));
			e = doc.getRootElement().getChild("Data").getChild("ColorInt");
		
			if (e!=null){
				index = e.getAttribute("ID").getIntValue();
				
				if (n)	return(numbers[index]);
				else
					return(word[index]);
			}
		} catch (JDOMException e1) {
			  jlogger.warning("Fehler beim Extensions XML Packet" + e1);
		} catch (IOException e1) {
			  jlogger.warning("Fehler beim Extensions XML Packet" + e1);
		} catch (ArrayIndexOutOfBoundsException e1){
			  jlogger.warning("Fehler beim Extensions XML Packet" + e1);
		}
	
		return n?"5":"Sonstige";
		}
	
}

