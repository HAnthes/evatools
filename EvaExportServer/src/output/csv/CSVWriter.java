package output.csv;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import java.util.function.Function;

import util.Setup;
import eva.controlller.IWriter;
import eva.model.Vehicle;
import eva.model.VehicleOptionCategory;

/**
 * Writer Listener implentation CSV Dateien.
 * 
 * Erzeugt CSV Dateien und verwaltet die Properties f�r die Inseraterneuerung von Moblie.de
 * 
 * @author anthes
 * @version 1 - 04.01.2017
 * 
 */
public class CSVWriter implements IWriter {

	private static java.util.logging.Logger jlogger = java.util.logging.Logger.getLogger(Class.class.getName());

	
	public void writer(List<Vehicle> cars, List<VehicleOptionCategory> evoOptions) {
		
		Properties pLoad = loadProperties(Setup.getMobileReNewlog());
		Properties pSave = new Properties();
		
		List<CarDate> renew = new ArrayList<CarDate>();
				
		cars.forEach(c -> checkDate(c, pLoad, pSave, renew, Setup.getMobileReNewTrigger()));
		saveProperites(pSave, Setup.getMobileReNewlog());
		
		
		jlogger.info("Schreiben von :" + Setup.getCsv_mobile());
		out(cars, (Vehicle auto) -> CSVMapMobile.map(auto, renew), Setup.getCsv_mobile());
		
		jlogger.info("Schreiben von :" + Setup.getCsv_As24());
		out(cars, (Vehicle auto) -> CSVMapAutoscout24.map(auto), Setup.getCsv_As24());
	}
	

	/*
	 * Hilfsmethode zu erzeugen der CSV Dateien
	 */
	private void out(List<Vehicle> cars, Function<Vehicle, String> mapper, String dateiname) {
		File datei = new File(dateiname);
		try {
			FileWriter writer = new FileWriter(datei);
			cars.stream().map(mapper).forEach(s -> {
			try {
				writer.write(s);
			} catch (IOException e) {
				jlogger.warning("Fehler beim Schreiben in " + dateiname + " " + e);
			}
			});

			writer.flush();
			writer.close();
		} catch (IOException e) {
			jlogger.warning("Fehler beim erstellen von " + dateiname + " " + e);
		}
	}
	
	
	/*
	 * Hilfsmethode Laden der Properties f�r die Erneuerung 
	 */
	private static Properties loadProperties(String fname){
		Properties pLoad = new Properties();
		try {
			BufferedInputStream stream = new BufferedInputStream(new FileInputStream(fname));
			jlogger.info("ReNew.Propiertes geladen " + fname);
			pLoad.load(stream);
			stream.close();
		} catch (Exception e) {
			jlogger.warning("Fehler beim lesen der ReNew.Properties"  + e);
		}
		return pLoad;
	}
	
	
	/*
	 * Hilfsmethode Speichern Properties
	 */
	private static void saveProperites(Properties pSave, String fname){
		try {
			FileOutputStream out = new FileOutputStream(fname);
			jlogger.info("ReNew.Propiertes geschrieben " + fname);
			pSave.store(out,"-Autoupdate-");
			out.close();
		} catch (Exception e) {
			jlogger.warning("Fehler beim schreibe der ReNew.Properties"  + e);
		}
	}
	

	/*
	 * Hilfsmethode Auswerten der Properties
	 * Ist FZ noch nicht in Properties - erzeugen mit Create Datum des FZ
	 * Ist FZ vorhand und Trigger kleiner Datum von "jetzt" verwenden und FZ in Renew Liste schreiben.
	 */
	private static void checkDate(Vehicle car,Properties pLoad, Properties pSave,  List<CarDate> renew, long outofDate){
		
		GregorianCalendar now = new GregorianCalendar();
		CarDate c = new CarDate();
		
		//Neues FZ
		if(pLoad.getProperty(car.getVehicleNo().toString())==null){
			jlogger.info("Fahrzeug neu : " + car.getVehicleNo());
			pSave.setProperty(car.getVehicleNo().toString(), String.valueOf(car.getDateCreated().getTime()));
			c.setRenew(false);
			c.setFzno(car.getVehicleNo().toString());
			c.setDateinmilis(car.getDateCreated().getTime());
		} else {
			//vorhandenes FZ
			Long pDate = Long.parseLong(pLoad.getProperty(car.getVehicleNo().toString()));
			Long isDate = now.getTimeInMillis();
			//Datum aus dem geladenen Properties kopieren
			pSave.setProperty(car.getVehicleNo().toString(), pLoad.getProperty(car.getVehicleNo().toString())); 
			
			c.setRenew(false);
			c.setFzno(car.getVehicleNo().toString());
			c.setDateinmilis(pDate);
			
			long daydiff = (isDate-pDate)/ (1000 * 60 * 60 * 24);
			//ist zu erneuern - jetzt als Datum in Properties
			if (daydiff > outofDate){
				jlogger.info("ReNew :" + car.getVehicleNo().toString() + "Diff : " + daydiff + " : Trigger : " + outofDate );
				pSave.setProperty(car.getVehicleNo().toString(),String.valueOf(isDate));
				c.setDateinmilis(isDate);
				c.setRenew(true);
			}
		}
		renew.add(c);
	}
	
	
}
