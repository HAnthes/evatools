package output.csv;
/*
 * Fast FZ Nr f�r Renew und letztes Datum zusammen.
 */
public class CarDate {
	
	private String fzno="";
	private long dateinmilis=0;
	private boolean renew=false;
	
	public CarDate(){}
	
	public String getFzno() {
		return fzno;
	}

	public void setFzno(String fzno) {
		this.fzno = fzno;
	}

	public long getDateinmilis() {
		return dateinmilis;
	}

	public void setDateinmilis(long dateinmilis) {
		this.dateinmilis = dateinmilis;
	}

	public boolean isRenew() {
		return renew;
	}

	public void setRenew(boolean renew) {
		this.renew = renew;
	}

	@Override
	public String toString() {
		return "CarDate [fzno=" + fzno + ", dateinmilis=" + dateinmilis + ", renew=" + renew + "]";
	}

	

	
}
