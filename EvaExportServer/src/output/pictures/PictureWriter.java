package output.pictures;

import java.util.List;

import eva.controlller.IWriter;
import eva.model.Vehicle;
import eva.model.VehicleOptionCategory;

public class PictureWriter implements IWriter {
	private java.util.logging.Logger jlogger = java.util.logging.Logger.getLogger(Class.class.getName());
	@Override
	public void writer(List<Vehicle> cars,
			List<VehicleOptionCategory> evoOptions) {
		// TODO Auto-generated method stub
		jlogger.info("Bilder bearbeiten");
		MakePictures.makePics(cars);
	}
	

}
