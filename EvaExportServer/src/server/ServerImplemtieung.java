package server;


import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import transfer.Worker;
import util.Setup;
import common.CallbackInterface;
import common.WorkerInterface;

/**
 * Server Implementierung f�r den RMI Stub.
 * @author anthes
 *
 */

public class ServerImplemtieung implements WorkerInterface, PrinterInterface{
	
	private static java.util.logging.Logger jlogger = java.util.logging.Logger.getLogger(Class.class.getName());
	
	private List<CallbackInterface>listener = new ArrayList<CallbackInterface>();
	
	@Override
	//TODO : �bergabe von SEtupinfos aufteilen!
	public Collection<String> getSetup() throws RemoteException {
		ArrayList<String> setup = new ArrayList<String>();
		setup.add("Mobile Buchungswiederholung : " + Setup.isMobilerepeat());
		setup.add("Mobile Blickf�nger : " + Setup.getMobileBlickfStart() + " - " + Setup.getMobileBlickfEnde() + " Gesamt : " + Setup.getCountMobileBlickf());
		setup.add("Mobile Seite 1 : " + Setup.getMobileErsteSeiteStart() + " - " + Setup.getMobileErsteSeiteEnde()+ " Gesamt : " + Setup.getCountMobileErsteSeite());
		setup.add("Mobile Ebay : Preis " + Setup.getMobileEbayPreis() + ", Standtage " + Setup.getMobileEbayStart() + " - " + Setup.getMobileEbayEnde() + " KM Limit : " + Setup.getMobileEbayKM() + " Gesamt : " + Setup.getCountMobileEBay());
		setup.add("Mobile Renew Zyklus : " + Setup.getMobileReNewTrigger() + " Gesamt : " + Setup.getCountMobileReNew());
		setup.add("AS24 Premium: " + Setup.getAs24PremiumStart() + " - " +Setup.getAs24PremiumEnde() + " Gesamt : " + Setup.getCountAS24Premium());
		setup.add("AS24 Plus: " + Setup.getAs24PlusStart() + " - " +Setup.getAs24PlusEnde() + " Gesamt : " + Setup.getCountAS24Plus());		
		return setup;
	}

	
	@Override
	public boolean getStatus() throws RemoteException {
		return Worker.getWork();
	}
	
	//@todo : warum worker als Threat?
	@Override 
	public void startTransfer() throws RemoteException {
		jlogger.info("Versuche Transfer zu starten");
		if(!Worker.getWork()){
			jlogger.info("Starte Transfgerthread");
			Thread worker = new Thread(new Worker());
			worker.start();
		} else
		{
			jlogger.info("Transferthread l�uft bereits");
			
		}
		jlogger.info("Transferthread beendet oder nicht gestartet");
		print("�bertragung abgschlossen");
	}


	@Override
	public synchronized void registerCallback(CallbackInterface cbi) throws RemoteException {
		jlogger.info("Registere Callbacklistener");
		listener.add(cbi);
		
	}

	@Override
	public synchronized void deregisterCallback(CallbackInterface cbi)
			throws RemoteException {
		jlogger.info("l�sche Callbacklistener");
		listener.remove(cbi);
		
	}

	
	public synchronized void print(String text){
		for(CallbackInterface i : listener){
			try{
			  i.printer(text);
			} catch (RemoteException e){
				listener.remove(i);
				jlogger.warning("Callback Fehler : l�schen " + listener.size() + "Fehler  :" +   e );
			}
		}
	}

	@Override
	public long getLastTransfer() throws RemoteException {
		return Setup.getLastExport();
	}

	@Override
	public void setLastTransfer(long datumlong) throws RemoteException {
		//Setzten eines Datums um zur�ckliegende Bilder wieder zu "erfassen" 
	}
	
}
