package transfer;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.CountDownLatch;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import util.RemoteWriter;

/**
 * Apache HTTP Client
 * Mobile.de Basic Auth Upload API http://services.mobile.de/manual/upload-interface_de.html
 * Properties mit type:ftp oder http erweitert  
 * @author anthes
 *
 */

public class HTTPClientPostTransfer implements Runnable {
	private static java.util.logging.Logger jlogger = java.util.logging.Logger.getLogger(Class.class.getName());
	
	private static RemoteWriter rw = RemoteWriter.getInstance();
	

	private TransferSetup transferSetup;
	private CountDownLatch sync;

	public HTTPClientPostTransfer(TransferSetup transferSetup, CountDownLatch sync) {
		this.transferSetup = transferSetup;
		this.sync = sync;
	}


	/**
	 * Postrequest f�r ein Datei
	 */
	private void uploadFile() {
		//Datei und Contenttype
		File f = new File(transferSetup.getFilename());
		/**
		 * Listener f�r Upload Info.
		 */
		FileEntity payload = new FileEntity(f){
			@Override
		    public void writeTo(OutputStream outstream) throws IOException {
		        super.writeTo(new BufferedOutputStream(outstream){
		            int writedBytes = 0;
		            @Override
		            public synchronized void write(byte[] b, int off, int len) throws IOException {
		                super.write(b, off, len);
		                writedBytes+=len;
		                rw.print("HTTP Post: "+writedBytes+"/"+getContentLength() + " : " + f.getName());
		            }
		        });
		    }
		};
	
		payload.setContentType("binary/octet-stream");
		String filename = transferSetup.getName();
		
		//Client erstellen
		CredentialsProvider credsProvider = new BasicCredentialsProvider();
	    credsProvider.setCredentials(
	                new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT, AuthScope.ANY_REALM, "basic"),
	                new UsernamePasswordCredentials(transferSetup.getUser(),transferSetup.getPassword()));
	     														   
	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
		
	    
	    HttpPost post = new HttpPost(transferSetup.getServer() + filename );
	    post.setEntity(payload);
	    
	    rw.print("HTTP Thread " + Thread.currentThread().getId() + "Request " + post.getRequestLine());
		jlogger.info("HTTP Thread " + Thread.currentThread().getId() + "Request " + post.getRequestLine());
		
		HttpResponse response=null;
		try {
			response = httpclient.execute(post);
			 //  HttpEntity resEntity = response.getEntity();
			   rw.print("HTTP Thread " + Thread.currentThread().getId() + "Request " + response.getStatusLine());
			   jlogger.info("HTTP Thread " + Thread.currentThread().getId() + "Request " + response.getStatusLine());
			 httpclient.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			jlogger.info("HTTP Thread " + e);
			rw.print("HTTP Thread " +e );
		}
	     
	
	}


	private void submit() {
		
		uploadFile();
		clean();
		
	}

	private void clean() {
		if (transferSetup.getClear().equals("yes")) {
			if (!transferSetup.getFilename().equals("none")) {
				File dfile = new File(transferSetup.getFilename());
				if (dfile.exists()) {
					jlogger.info("Clean up delete " + dfile.getAbsolutePath());
					rw.print("Aufr�umen :  " + dfile.getAbsolutePath());
					dfile.delete();
				}
			}
			File directory = new File(transferSetup.getSource());
			for (File file : directory.listFiles()) {
				jlogger.info("Clean up delete " + file.getAbsolutePath());
				rw.print("Aufr�umen :  " + file.getAbsolutePath());
				file.delete();
			}
		}
	}

	@Override
	public void run() {
		jlogger.info(Thread.currentThread().getId() + " Start" + sync.toString());
		this.submit();
		sync.countDown();
		jlogger.info(Thread.currentThread().getId() + " Ende" + sync.toString());
	}
}
