package transfer;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.client.fluent.Request;

import util.RemoteWriter;
import util.Setup;
/*
 * Erzeugen der Setupup Objekte und der Arbeitsthreads
 * http://services.mobile.de/manual/upload-interface_de.html -> unterscheidung in ftp und http 
 * http Methode an mobile.de angepasst.
 */
public class Transfer {

	private static java.util.logging.Logger jlogger = java.util.logging.Logger.getLogger(Class.class.getName());
	private static RemoteWriter rw = RemoteWriter.getInstance();

	public static void submit(String setup){
		
		List<TransferSetup> transferSetup = ReadExportSetup.read(setup);
		CountDownLatch sync = new CountDownLatch(transferSetup.size());
		ExecutorService pool = Executors.newCachedThreadPool();
		
		for (TransferSetup s : transferSetup) {
			if (!s.getFilename().equals("none")) {
				jlogger.info("Zipper Start : "  + s.getName());
				rw.print("Zipper : "  + s.getName());
				Zipper.createZipArchive(s.getSource(), s.getFilename());
			}

			if(s.getType().equals("ftp")){
				jlogger.info("HTTP :" + s.getName());
				pool.submit(new FTPTransfer(s,sync));	
			}
			
			if(s.getType().equals("http")){
				jlogger.info("HTTP :" + s.getName());
				pool.submit(new HTTPClientPostTransfer(s,sync));	
			}
		}
		pool.shutdown();
		try {
			sync.await();
			} catch (InterruptedException e) {
			jlogger.warning("Threadsycn Transfer" +e);
		} 
		jlogger.info("Transfer Ende!");
		try{
			String http = Request.Get(Setup.getService()).connectTimeout(90000).socketTimeout(90000).execute().returnContent().asString();
			rw.print("Abruf : "  + Setup.getService() + ":" + http);
		} catch(Exception e) {
			jlogger.info("Fehler beim Abruf des Service : "  + Setup.getService() + " : " +  e);
			rw.print("Fehler beim Abruf des Service : "  + Setup.getService());
		}			
		}
	}
