package transfer;

import java.io.IOException;
import java.util.Date;

import output.access.AccessWriter;
import output.csv.CSVWriter;
import output.pictures.PictureWriter;
import util.Setup;
import eva.controlller.EvaDBController;

public class Worker implements Runnable {
	private static java.util.logging.Logger jlogger = java.util.logging.Logger.getLogger(Class.class.getName());
		
	private static boolean work = false;

	
	public Worker(){
		
	}
	
	public static synchronized boolean getWork() {
		return work;
	}

	private static synchronized void working() {
		work = true;
	}

	private static synchronized void notworking() {
		work = false;
	}

	public static void transfer() {
		
		working();
		try {
			jlogger.info("Server : Setup laden");
			
			Setup.makeSetup();
			Setup.setLastExport(new Date().getTime());
			
			//Ausgabe Methoden direkt an die  DB Controller gebunden. Daten werden nach mdem erstellen nicht mehr ben�tigt
			EvaDBController en =  new EvaDBController(new CSVWriter(),new AccessWriter(),new PictureWriter() );
			en.execute();
			
			jlogger.info("Server : Transfer Start!");
			Transfer.submit(Setup.getTransfer());

		
		} catch (IOException e1) {
			System.out.println("Setup Datei nicht gefunden!" + e1);
		}
		jlogger.info("Beendet");
		notworking();
		Setup.setLastExport(new Date().getTime());
	}

	@Override
	public void run() {
		
		if(!getWork()) transfer();
		
	}
}
