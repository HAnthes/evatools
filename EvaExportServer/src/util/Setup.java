package util;



import java.io.FileInputStream;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Properties;
/**
 * Configurations Klasse.
 * L�dt Propertiesdatei
 * makeSetup() default Properties File 
 * makeSetup(propfile) f�r spezialf�lle - Testlauf
 * 
 * @author anthes
 * 
 *
 */
public class Setup {

	private static String accessTempl;
	private static String accessWork;
	private static String mobileCSV;
	private static String csv_As24;
	private static String eVAPic;
	private static long lastExport=0;
	private static String picxml;

	private static long mobileErsteSeiteStart;
	private static long mobileErsteSeiteEnde;
	private static long mobileEbayStart;
	private static long mobileEbayEnde;
	private static long mobileEbayPreis;
	private static long mobileEbayKM;
	private static long mobileBlickfStart;
	private static long mobileBlickfEnde;
	private static String mobileCSVText="";
	private static String mobileReNewlog="";
	private static long mobileReNewTrigger;
	
	private static String autoscoutCSVText="";
	private static String transfer="";
	private static String service;
	private static String pichistorie;
	private static long as24PremiumStart;
	private static long as24PremiumEnde;
	private static long as24PlusStart;
	private static long as24PlusEnde;
	private static String evaprop;
	
	private static int countMobileErsteSeite=0;
	private static int countMobileEBay=0;
	private static int countMobileBlickf=0;
	private static int countAS24Premium=0;
	private static int countAS24Plus=0;
	private static int countMobileReNew=0;
	
	private static boolean mobilerepeat=false;
	
	
	
	private static java.util.logging.Logger jlogger = java.util.logging.Logger.getLogger(Class.class.getName());
	/**
	 * L�dt Standart Properties File per default
	 * @throws IOException
	 */
	public static void makeSetup() throws IOException {
		 makeSetup(System.getProperty("user.dir") + "\\eva.properties");
	}
	
	/**
	 * L�dt Propfile - f�r zb. Tests
	 * @param propfile
	 * @throws IOException
	 */
	public static void makeSetup(String propfile) throws IOException {
		jlogger.info("Lader der Setupproperties : " + propfile );
		evaprop = propfile;
		readProp();
	}
	
	/**
	 * L�dt Propertiesfile  
	 * @throws IOException
	 */
	public static void readProp() throws IOException{
		Properties properties = new Properties();
		BufferedInputStream stream = new BufferedInputStream(new FileInputStream(evaprop));
		properties.load(stream);
		stream.close();
		accessTempl = properties.getProperty("AccessFileTemplate");
		accessWork = properties.getProperty("AccessFileOutPut");
		mobileCSV = properties.getProperty("Mobile_CSV");
		mobileErsteSeiteStart = Long.parseLong(properties.getProperty("Mobile_Seite1Start"),10);
		mobileErsteSeiteEnde= Long.parseLong(properties.getProperty("Mobile_Seite1Ende"),10);
		mobileBlickfStart = Long.parseLong(properties.getProperty("Mobile_BlickfStart"),10);
		mobileBlickfEnde = Long.parseLong(properties.getProperty("Mobile_BlickfEnde"),10);
		mobileEbayStart = Long.parseLong(properties.getProperty("Mobile_EbayStart"),10);
		mobileEbayEnde= Long.parseLong(properties.getProperty("Mobile_EbayEnde"),10);
		mobileEbayPreis= Long.parseLong(properties.getProperty("Mobile_EbayPreis"),10);
		csv_As24 = properties.getProperty("CSV_AutoScout");
		eVAPic = properties.getProperty("EVAPic");
		picxml=properties.getProperty("PicXML");
		autoscoutCSVText=properties.getProperty("Autoscout_CSV_Text");
		transfer=properties.getProperty("transfer");
		mobileCSVText=properties.getProperty("Mobile_CSV_Text");
		service= properties.getProperty("service");
		pichistorie=properties.getProperty("History");
		as24PremiumStart = Long.parseLong(properties.getProperty("AS24PremiumStart"),10);
		as24PremiumEnde = Long.parseLong(properties.getProperty("AS24PremiumEnde"),10);
		as24PlusStart = Long.parseLong(properties.getProperty("AS24PlusStart"),10);
		as24PlusEnde = Long.parseLong(properties.getProperty("AS24PlusEnde"),10);
		mobileReNewlog = properties.getProperty("Mobile_ReNewlog"); //Speicher die Standtage nach �bertragung oder inseratserneuerung.
		mobileReNewTrigger = Long.parseLong(properties.getProperty("Mobile_ReNewTrigger"),10);
		mobileEbayKM = Long.parseLong(properties.getProperty("Mobile_EbayKM"),10);
		mobilerepeat = properties.get("Mobile_Repeat").equals("y");
	}
	
	public static long getMobileEbayKM() {
		return mobileEbayKM;
	}

	public static long getMobileReNewTrigger() {
		return mobileReNewTrigger;
	}

	public static String getMobileReNewlog() {
		return mobileReNewlog;
	}


	public static long getMobileEbayStart() {
		return mobileEbayStart;
	}
	public static long getMobileEbayEnde() {
		return mobileEbayEnde;
	}
	public static long getMobileEbayPreis() {
		return mobileEbayPreis;
	}
	public static String getPichistorie() {
		return pichistorie;
	}
	public static String getService() {
		return service;
	}
	
	public static long getMobileErsteSeiteStart() {
		return mobileErsteSeiteStart;
	}

	public static long getMobileErsteSeiteEnde() {
		return mobileErsteSeiteEnde;
	}

	public static long getMobileBlickfStart() {
		return mobileBlickfStart;
	}

	public static long getMobileBlickfEnde() {
		return mobileBlickfEnde;
	}

	public static String getAccessTempl() {
		return accessTempl;
	}

	public static String getAccessWork() {
		return accessWork;
	}

	public static String getCsv_mobile() {
		return mobileCSV;
	}

	public static String getCsv_As24() {
		return csv_As24;
	}

	public static String geteVAPic() {
		return eVAPic;
	}

	public static String geteVAPic(String datei) {
		return eVAPic + "/" + datei;
	}
	
	public static String getpicxml() {
		return picxml;
	}
	
	public static String getMobileCSVText() {
		return mobileCSVText;
	}

	public static String getAutoscoutCSVText() {
		return autoscoutCSVText;
	}

	public static String getTransfer() {
		return transfer;
	}
	
	public static long getAs24PremiumStart() {
		return as24PremiumStart;
	}
	public static long getAs24PremiumEnde() {
		return as24PremiumEnde;
	}
	public static long getAs24PlusStart() {
		return as24PlusStart;
	}
	public static long getAs24PlusEnde() {
		return as24PlusEnde;
	}
		
	//Halten den Timestamp der letzten Aktion. bei Serverstart noch 0
	public static long getLastExport(){
		 return lastExport;
	}
	
	public static void setLastExport(long last) {
		lastExport = last;
	}

	public static int getCountMobileErsteSeite() {
		return countMobileErsteSeite;
	}

	public static void incCountMobileErsteSeite() {
		Setup.countMobileErsteSeite++;
	}

	public static int getCountMobileEBay() {
		return countMobileEBay;
	}

	public static void incCountMobileEBay() {
		Setup.countMobileEBay++;
	}

	public static int getCountMobileBlickf() {
		return countMobileBlickf;
	}

	public static void incCountMobileBlickf() {
		Setup.countMobileBlickf++;
	}

	public static int getCountAS24Premium() {
		return countAS24Premium;
	}

	public static void incCountAS24Premium() {
		Setup.countAS24Premium++;
	}

	public static int getCountAS24Plus() {
		return countAS24Plus;
	}

	public static void incCountAS24Plus() {
		Setup.countAS24Plus++;
	}

	public static int getCountMobileReNew() {
		return countMobileReNew;
	}

	public static void incCountMobileReNew() {
		Setup.countMobileReNew++;
	}

	public static boolean isMobilerepeat() {
		return mobilerepeat;
	}
	
	
		
}
