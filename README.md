#  Exportool für Volkswagen EVA Gebrauchtfahzeuge und Bilder zu GW-Börsen zu exportieren. #
 
### Benötigt ###

* EVA System und Zugriff zur dessen Datenbank.
* Zugriff auf Internet
* Vorhandene Konten bei Autoscout, Mobile usw die für FTP Upload konfiguriert sind.

### Vorarbeiten ###

* eva.properties anpassen
* server.properties anpassen
* Bilderverarbeitung einstellen (XML) für alle benötigten Bilder
* FTP Transfer Properties erzeugen.
* Im Ordner Installation liegt hier lauffähige Installation mit Konfiguration für AS24, Mobile und einem eigenen Webserver.

### Starten ###

* Bei Systmen mit mehr als einen Netzadaper mit -Djava.rmi.server.hostname den RMI Host festlegen.
